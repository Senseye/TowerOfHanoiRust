use std::{u8, usize};

pub struct Tower {
    disks: Vec<u8>,
}

impl Tower {
    pub fn new(disks: Vec<u8>) -> Tower {
        return Tower { disks };
    }

    pub fn size(&self) -> usize {
        return self.disks.len();
    }

    pub fn empty(&self) -> bool {
        return self.size() == 0;
    }

    pub fn top(&self) -> u8 {
        return self.disks[self.disks.len() - 1];
    }

    pub fn put(&mut self, disk: u8) {
        self.disks.push(disk);
    }

    pub fn take(&mut self) -> u8 {
        return self.disks.pop().unwrap();
    }

    pub fn hash(&self) -> String {
        let mut hash: String = "".to_owned();
        let delimiter = ",";
        for disk in self.disks.iter() {
            hash = hash + &disk.to_string() + delimiter;
        }
        return hash;
    }

    pub fn clone(&self) -> Tower {
        let disks = self.disks.clone();

        return Tower { disks: disks };
    }
}

#[cfg(test)]
mod tower_test {
    use tower::Tower;

    #[test]
    fn empty_tower() {
        let tower = Tower::new(vec![]);
        assert!(tower.empty());
    }

    #[test]
    fn tower() {
        let tower = Tower { disks: vec![3, 2, 1] };
        assert_eq!(tower.top(), 1);
        assert_eq!(tower.empty(), false);
    }

    #[test]
    fn tower_put() {
        let mut tower = Tower { disks: vec![] };
        tower.put(3);
        assert_eq!(tower.top(), 3);
        tower.put(2);
        assert_eq!(tower.top(), 2);
    }

    #[test]
    fn tower_take() {
        let mut tower = Tower { disks: vec![3, 2, 1] };
        assert_eq!(tower.take(), 1);
        assert_eq!(tower.take(), 2);
        assert_eq!(tower.take(), 3);
    }

    #[test]
    fn tower_empty_hash() {
        let tower = Tower { disks: vec![] };

        assert_eq!(tower.hash(), "");
    }

    #[test]
    fn tower_hash() {
        let tower = Tower { disks: vec![3, 2, 1] };

        assert_eq!(tower.hash(), "3,2,1,");
    }

    #[test]
    fn tower_clone() {
        let source_tower = Tower { disks: vec![8, 7] };

        let mut clone_tower = source_tower.clone();

        assert_eq!(source_tower.hash(), "8,7,");
        assert_eq!(clone_tower.hash(), "8,7,");

        clone_tower.take();

        assert_eq!(source_tower.hash(), "8,7,");
        assert_eq!(clone_tower.hash(), "8,");
    }
}
