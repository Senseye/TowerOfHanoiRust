use std::{u8, usize};
use std::string::ToString;

#[derive(Copy)]
pub struct Action {
    pub from: usize,
    pub to: usize,
    pub disk: u8,
}

impl Action {
    pub fn new(from: usize, to: usize, disk: u8) -> Action {
        return Action { from, to, disk };
    }
}

impl ToString for Action {
    fn to_string(&self) -> String {
        return format!(
            "move {disk} from {from} to {to}",
            disk = self.disk,
            from = self.from,
            to = self.to
        );
    }
}

impl Clone for Action {
    fn clone(&self) -> Action { return *self; }
}

#[cfg(test)]
mod action_test {
    use action::Action;

    #[test]
    fn test_to_string() {
        let action = Action::new(0, 2, 1);

        assert_eq!(action.to_string(), "move 1 from 0 to 2");
    }
}
