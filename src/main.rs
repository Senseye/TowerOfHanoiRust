use std::{u8, u32};
use std::vec::Vec;
use std::string::String;
use std::collections::HashMap;

use std::io::prelude::*;
use std::fs::File;

use std::time::SystemTime;

mod tower;

use tower::Tower;

mod state;

use state::State;

mod action;

use action::Action;

const TOWER_SIZE: u8 = 7;

fn main() {
    let language = "Rust";

    let output: String = format!(
        "Tower of Hanoi by {language}",
        language = language
    );

    println!("{}", output);

    let size = 8;

    let begin = State::new(
        vec![
            StateGenerator::get_filled_tower(size),
            StateGenerator::get_empty_tower(),
            StateGenerator::get_empty_tower(),
            StateGenerator::get_empty_tower(),
        ]
    );

    let end = State::new(
        vec![
            StateGenerator::get_empty_tower(),
            StateGenerator::get_empty_tower(),
            StateGenerator::get_empty_tower(),
            StateGenerator::get_filled_tower(size),
        ]
    );

    let mut search = BreadthFirstSearch::create(begin, end);

    let start_time = SystemTime::now();
    search.solve();
    let difference = SystemTime::now().duration_since(start_time);
    println!("{:?}", difference);

    ActionWriter::write("mainBreadthFirstSearchOutput.txt".to_string(), search.actions);
}

struct ChangeStateAction {
    from: State,
    to: State,
    action: Action,
}

struct CommonSearch {
    target: State,
    past_state_map: HashMap<String, bool>,
    tries: u32,
}

impl CommonSearch {
    fn new(end: State) -> CommonSearch {
        return CommonSearch {
            target: end,
            past_state_map: HashMap::new(),
            tries: 0,
        };
    }

    fn same(&mut self, hash: String) -> bool {
        self.tries += 1;

        return self.target.hash() == hash;
    }

    fn get_possible_change_state_actions(&mut self, state: State) -> Vec<ChangeStateAction> {
        let mut result = vec![];
        let size = state.len();

        for from_index in 0..size {
            for to_index in 0..size {
                if from_index != to_index {
                    if state.can_move_between(from_index, to_index) {
                        let mut possible_state = state.clone();
                        let disk = possible_state.move_between(from_index, to_index);
                        let hash = possible_state.hash();

                        if self.is_past_state(hash.clone()) {
                            continue;
                        }

                        self.add_past_state(hash.clone());

                        let action = Action::new(
                            from_index,
                            to_index,
                            disk,
                        );

                        result.push(ChangeStateAction {
                            from: state.clone(),
                            to: possible_state,
                            action,
                        });
                    }
                }
            }
        }

        return result;
    }

    fn add_past_state(&mut self, hash: String) {
        self.past_state_map.insert(hash, true);
    }

    fn is_past_state(&self, hash: String) -> bool {
        return self.past_state_map.contains_key(&hash);
    }
}

struct BreadthFirstSearch {
    begin: State,
    search: CommonSearch,
    actions: Vec<Action>,
    action_change_history: HashMap<String, ChangeStateAction>,
}

struct DeepFirstSearch {
    begin: State,
    search: CommonSearch,
    actions: Vec<Action>,
}

impl DeepFirstSearch {
    fn create(begin: State, end: State) -> DeepFirstSearch {
        return DeepFirstSearch {
            begin,
            search: CommonSearch::new(end),
            actions: vec![],
        };
    }

    fn solve(&mut self) -> bool {
        let state = self.begin.clone();

        let result = self.solve_recursive(state);

        if result {
            self.actions.reverse();
        }

        return result;
    }

    fn solve_recursive(&mut self, state: State) -> bool {
        if self.search.same(state.hash().clone()) {
            return true;
        }

        let change_state_actions = self.search.get_possible_change_state_actions(state.clone());

        for change_state_action in change_state_actions {
            let found = self.solve_recursive(change_state_action.to);

            if found {
                self.actions.push(change_state_action.action);
                return true;
            }
        }

        return false;
    }
}

impl BreadthFirstSearch {
    fn create(begin: State, end: State) -> BreadthFirstSearch {
        return BreadthFirstSearch {
            begin,
            search: CommonSearch::new(end),
            actions: vec![],
            action_change_history: HashMap::new(),
        };
    }

    fn solve(&mut self) -> bool {
        let found = self.queue_search();

        if found {
            let mut current = &self.search.target;
            while self.action_change_history.contains_key(&current.hash()) {
                let action = self.action_change_history.get(&current.hash()).unwrap();

                self.actions.push(action.action.clone());

                current = &action.from;
            }

            self.actions.reverse();
        }

        return found;
    }

    fn queue_search(&mut self) -> bool {
        let mut stack: Vec<State> = vec![];
        let state = self.begin.clone();
        self.search.add_past_state(state.hash().clone());
        stack.push(state);

        while stack.len() > 0 {
            let current_state = stack.pop().unwrap();

            if self.search.same(current_state.hash()) {
                return true;
            }

            let change_state_actions = self.search.get_possible_change_state_actions(current_state.clone());

            for change_state_action in change_state_actions {
                let next_state = change_state_action.to.clone();
                self.action_change_history.insert(
                    next_state.hash(),
                    change_state_action,
                );
                stack.push(next_state);
            }
        }

        return false;
    }
}

struct ActionWriter;

impl ActionWriter {
    fn write(file_name: String, actions: Vec<Action>) {
        let mut handler = File::create(file_name.to_string());
        let mut file = handler.ok().unwrap();

        for action in actions {
            file.write_all(action.to_string().as_bytes());
            file.write_all(b"\n");
        }
    }
}

struct StateGenerator;

impl StateGenerator {
    fn get_filled_tower(size: u8) -> Tower {
        let mut disks = vec![];
        let to = size + 1;
        let reverse = (1..to).rev();

        for i in reverse {
            disks.push(i);
        }

        return Tower::new(disks);
    }

    fn get_empty_tower() -> Tower {
        return Tower::new(vec![]);
    }

    fn begin() -> State {
        return State::new(
            vec![
                StateGenerator::get_filled_tower(TOWER_SIZE),
                StateGenerator::get_empty_tower(),
                StateGenerator::get_empty_tower(),
            ]
        );
    }

    fn end() -> State {
        return State::new(
            vec![
                StateGenerator::get_empty_tower(),
                StateGenerator::get_empty_tower(),
                StateGenerator::get_filled_tower(TOWER_SIZE),
            ]
        );
    }

    fn validate_action_history(actions: Vec<Action>, from: State, to: State) -> bool {
        if actions.is_empty() {
            return false;
        }

        let mut current = from.clone();
        for action in actions {
            let success = current.can_move_between(action.from, action.to)
                && current.move_between(action.from, action.to) == action.disk;

            if success == false {
                return false;
            }
        }

        return current.hash() == to.hash();
    }
}

#[cfg(test)]
mod breadth_first_search_test {
    use BreadthFirstSearch;
    use StateGenerator;
    use ActionWriter;
    use action::Action;

    #[test]
    fn test_solve() {
        let mut search = BreadthFirstSearch::create(
            StateGenerator::begin(),
            StateGenerator::end(),
        );

        assert!(search.solve());
        assert_eq!(search.search.tries, 730);

        let mut actions: Vec<Action> = vec![];

        for action in search.actions.iter() {
            actions.push(action.clone());
        }

        assert!(StateGenerator::validate_action_history(
            search.actions,
            StateGenerator::begin(),
            StateGenerator::end(),
        ));

        ActionWriter::write(
            "breadthFirstSearchOutput.txt".to_string(),
            actions
        );
    }
}

#[cfg(test)]
mod deep_first_search_test {
    use DeepFirstSearch;
    use StateGenerator;
    use ActionWriter;
    use action::Action;

    #[test]
    fn test_solve() {
        let mut search = DeepFirstSearch::create(
            StateGenerator::begin(),
            StateGenerator::end(),
        );

        assert!(search.solve());
        assert_eq!(search.search.tries, 1823);

        let mut actions: Vec<Action> = vec![];

        for action in search.actions.iter() {
            actions.push(action.clone());
        }
        assert!(StateGenerator::validate_action_history(
            search.actions,
            StateGenerator::begin(),
            StateGenerator::end(),
        ));

        ActionWriter::write(
            "deepFirstSearchOutput.txt".to_string(),
            actions
        );
    }
}